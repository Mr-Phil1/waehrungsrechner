package eu.mrPhil.decorator;

import eu.mrPhil.IUmrechnen;
import eu.mrPhil.exception.NichtZustaendigException;

public class GebuehrDekorierer extends Dekorierer {


    public GebuehrDekorierer(IUmrechnen naechsterDekorierer) {
        super(naechsterDekorierer);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws NichtZustaendigException {


        if (variante.toLowerCase().startsWith("eur")) {
            System.out.println("Gebühr für " + variante + " von 5€.");
            return super.umrechnen(variante, betrag) != 0.0 ? super.umrechnen(variante, betrag) + 5.0 : 0.0;
        } else {
            System.out.println("Gebühr (0,5%) für " + variante + " von " + Math.round((betrag * 0.05) * 100.0) / 100.0 + "€.");
            return super.umrechnen(variante, (betrag * 1.05));
        }

    }
}
