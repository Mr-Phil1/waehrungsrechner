package eu.mrPhil.decorator;

import eu.mrPhil.IUmrechnen;
import eu.mrPhil.exception.NichtZustaendigException;

public abstract class Dekorierer implements IUmrechnen {
private final IUmrechnen naechsterWRDekorierer;

    public Dekorierer(IUmrechnen naechsterWRDekorierer) {
        this.naechsterWRDekorierer = naechsterWRDekorierer;
    }

    public double umrechnen(String variante, double betrag) throws NichtZustaendigException {
        if (this.naechsterWRDekorierer != null) {
            return naechsterWRDekorierer.umrechnen(variante, betrag);
        } else {
            System.out.println("Keine Zuständigkeit gefunden");
            return 0.0;
        }

    }
}
