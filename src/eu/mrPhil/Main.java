package eu.mrPhil;

import eu.mrPhil.adapter.AdapterSammelWR;
import eu.mrPhil.chainOfResponsibility.*;
import eu.mrPhil.decorator.GebuehrDekorierer;
import eu.mrPhil.exception.NichtZustaendigException;

public class Main {

    public static void main(String[] args) {
        IUmrechnen wr = new WREuro2Yen(new WREuro2Dollar(new WRYen2Euro(new WRDollar2Euro(new WRChf2Euro(new WREuro2Chf(null))))));
        wr = new GebuehrDekorierer(wr);
        ISammelumrechnung sammelWR = new AdapterSammelWR(wr);
        IUmrechnen iUmrechnen1 = new WREuro2Dollar.WREuro2DollarBuilder()
                .setNaechsterIUmrechner(new WRChf2Euro(null))
                .build();

        double[] dummyWerte = {5.5, 8, 88, 487, 9};
        double[] dummyWerte1 = {5.5, 8, 88, 35, 3};
        try {
            System.out.println("--------------------------------------------------");
            System.out.println("WR:");
            System.out.println(wr.umrechnen("Euro2Yen", 8888));
            System.out.println(wr.umrechnen("Yen2Euro", 8888));
            System.out.println("**************************************************");
            System.out.println(wr.umrechnen("Euro2Dollar", 590));
            System.out.println(wr.umrechnen("Dollar2Euro", 590));
            System.out.println(wr.umrechnen("Euro2Chf", 590));
            System.out.println(wr.umrechnen("Chf2Euro", 590));
            System.out.println("--------------------------------------------------");


            System.out.println("--------------------------------------------------");
            System.out.println("Builder WR:");
            System.out.println(iUmrechnen1.umrechnen("Dollar2Euro", 989));
            System.out.println(iUmrechnen1.umrechnen("Chf2Euro", 15145.11));
            System.out.println("--------------------------------------------------");

            System.out.println("--------------------------------------------------");
            System.out.println("Sammel WR:");
            System.out.println(sammelWR.sammelumrechnen(dummyWerte, "Dollar2Euro"));
            System.out.println(sammelWR.sammelumrechnen(dummyWerte, "Euro2Dollar"));
            System.out.println("**************************************************");
            System.out.println(sammelWR.sammelumrechnen(dummyWerte, "Yen2Euro"));
            System.out.println(sammelWR.sammelumrechnen(dummyWerte, "Euro2Yen"));
            System.out.println("--------------------------------------------------");
        } catch (NichtZustaendigException nichtZustaendigException) {
            System.out.println(nichtZustaendigException.getMessage());
        }

    }
}
