package eu.mrPhil;

import eu.mrPhil.exception.NichtZustaendigException;

public interface IUmrechnen {
    double umrechnen(String variante, double betrag) throws NichtZustaendigException;
}
