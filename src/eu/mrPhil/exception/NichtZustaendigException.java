package eu.mrPhil.exception;

public class NichtZustaendigException extends Exception {
    public NichtZustaendigException(String message) {
        super(message);
    }
}
