package eu.mrPhil;

import eu.mrPhil.exception.NichtZustaendigException;

public interface ISammelumrechnung {
    double sammelumrechnen(double[] betraege, String variante) throws NichtZustaendigException;
}
