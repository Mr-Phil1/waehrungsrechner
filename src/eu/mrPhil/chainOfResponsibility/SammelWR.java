package eu.mrPhil.chainOfResponsibility;

import eu.mrPhil.ISammelumrechnung;

public abstract class SammelWR implements ISammelumrechnung {
    private SammelWR naechsterSammelWR;

    public SammelWR(SammelWR naechsterSammelWR) {
        this.naechsterSammelWR = naechsterSammelWR;
    }

    @Override
    public double sammelumrechnen(double[] betraege, String variante) {
        if (this.naechsterSammelWR != null) {
            return naechsterSammelWR.sammelumrechnen(betraege, variante);
        } else {
            System.out.println("Keine Zuständigkeit gefunden");
            return 0.0;
        }
    }
}
