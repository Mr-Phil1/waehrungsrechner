package eu.mrPhil.chainOfResponsibility;

import eu.mrPhil.IUmrechnen;
import eu.mrPhil.exception.NichtZustaendigException;

public class WREuro2Chf extends WR {
    public WREuro2Chf(IUmrechnen naechsterWr) {
        super(naechsterWr);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws NichtZustaendigException {
        if (variante.equals("Euro2Chf")) {
            return super.umrechnen(betrag);
        } else {
            return super.umrechnen(variante, betrag);
        }

    }

    @Override
    public double getFaktor() {
        return 1.040;
    }

}
