package eu.mrPhil.chainOfResponsibility;

import eu.mrPhil.IUmrechnen;
import eu.mrPhil.exception.NichtZustaendigException;

public class WREuro2Yen extends WR {
    public WREuro2Yen(IUmrechnen naechsterWr) {
        super(naechsterWr);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws NichtZustaendigException {
        if (variante.equals("Euro2Yen")) {
            return super.umrechnen(betrag);
        } else {
            return super.umrechnen(variante, betrag);
        }

    }

    @Override
    public double getFaktor() {
        return 131.37;
    }

}
