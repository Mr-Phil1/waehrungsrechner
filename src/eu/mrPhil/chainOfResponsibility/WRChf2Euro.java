package eu.mrPhil.chainOfResponsibility;

import eu.mrPhil.IUmrechnen;
import eu.mrPhil.exception.NichtZustaendigException;

public class WRChf2Euro extends WR {
    public WRChf2Euro(IUmrechnen naechsterWr) {
        super(naechsterWr);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws NichtZustaendigException {
        if (variante.equals("Chf2Euro")) {
            return super.umrechnen(betrag);
        } else {
            return super.umrechnen(variante, betrag);
        }

    }

    @Override
    public double getFaktor() {
        return 0.95;
    }

}
