package eu.mrPhil.chainOfResponsibility;

import eu.mrPhil.IUmrechnen;
import eu.mrPhil.exception.NichtZustaendigException;

public class WRYen2Euro extends WR {
    public WRYen2Euro(IUmrechnen naechsterWr) {
        super(naechsterWr);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws NichtZustaendigException {
        if (variante.equals("Yen2Euro")) {
            return super.umrechnen(betrag);
        } else {
            return super.umrechnen(variante, betrag);
        }

    }

    @Override
    public double getFaktor() {
        return 0.0076;
    }

}
