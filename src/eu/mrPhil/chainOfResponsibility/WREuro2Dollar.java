package eu.mrPhil.chainOfResponsibility;

import eu.mrPhil.IUmrechnen;
import eu.mrPhil.exception.NichtZustaendigException;

public class WREuro2Dollar extends WR {

    public WREuro2Dollar(IUmrechnen naechsterWr) {
        super(naechsterWr);
    }

    @Override
    public double umrechnen(String variante, double betrag) throws NichtZustaendigException {
        if (variante.equals("Euro2Dollar")) {
            return super.umrechnen(betrag);
        } else {
            return super.umrechnen(variante, betrag);
        }

    }

    @Override
    public double getFaktor() {
        return 1.13;
    }

    public static class WREuro2DollarBuilder {
        private IUmrechnen naechsterIUmrechner;

        public WREuro2DollarBuilder() {
        }

        public WREuro2DollarBuilder setNaechsterIUmrechner(IUmrechnen iUmrechner) {
            this.naechsterIUmrechner = iUmrechner;
            return this;
        }

        public WREuro2Dollar build() {
            return new WREuro2Dollar(this.naechsterIUmrechner);
        }


    }
}
