package eu.mrPhil.chainOfResponsibility;

import eu.mrPhil.IUmrechnen;
import eu.mrPhil.exception.NichtZustaendigException;

public abstract class WR implements IUmrechnen {
    private final IUmrechnen naechsterWr;

    public WR(IUmrechnen naechsterWr) {
        this.naechsterWr = naechsterWr;
    }

    public double umrechnen(String variante, double betrag) throws NichtZustaendigException {
        if (this.naechsterWr != null) {
            return naechsterWr.umrechnen(variante, betrag);
        } else {
            throw new NichtZustaendigException("Keine Zuständigkeit gefunden");

        }

    }

    //Template Hook Pattern
    public double umrechnen(double betrag) {
        return Math.round((betrag * getFaktor()) * 100.0) / 100.0;
    }

    public abstract double getFaktor();
}
