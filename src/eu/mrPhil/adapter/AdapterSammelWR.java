package eu.mrPhil.adapter;

import eu.mrPhil.ISammelumrechnung;
import eu.mrPhil.IUmrechnen;
import eu.mrPhil.exception.NichtZustaendigException;

public class AdapterSammelWR implements ISammelumrechnung {
    private IUmrechnen iUmrechnen;

    public AdapterSammelWR(IUmrechnen iUmrechnen) {
        this.iUmrechnen = iUmrechnen;
    }


    @Override
    public double sammelumrechnen(double[] betraege, String variante) throws NichtZustaendigException {
        double sum = 0;
        for (int i = 0; i < betraege.length; i++) {
            sum += iUmrechnen.umrechnen(variante, betraege[i]);
        }
        return sum;
    }
}
